package cn.itcast.order.client;


import cn.itcast.order.pojo.User;
import org.springframework.stereotype.Component;

@Component
public class UserClientFallback implements UserClient {
    @Override
    public User findById(Long id) {
        User user = new User();
        user.setId(-1L);
        user.setUsername("查询失败");
        return user;
    }
}
