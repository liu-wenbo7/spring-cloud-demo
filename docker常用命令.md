# docker常用命令

- ##### yum下载docker		yum install docker -y 

- ##### 启动		systemctl start docker

- ##### 查看状态		systemctl status docker

- ##### 重启		systemctl restart docker

- ##### 关闭		systemctl stop docker

- ##### 查看所有镜像		docker images

- ##### 下载镜像		docker pull 镜像名称：镜像版本号

- ##### 删除镜像		docker rmi 镜像名

- ##### 查看正在运行的容器		docker ps

- ##### 查看所有镜像		docker ps -a

- ##### 启动容器		docker start 容器名称

- ##### 停止容器		docker stop 容器名称

- ##### 删除容器		docker rm 容器名称

- ##### 强制删除容器		docker rm -f 容器名称

- ##### 查看容器日志		docker logs 容器名称

- ##### 守护式创建容器		docker run -di --name=自定义名称  -p端口号：宿主机端口 镜像名称：镜像版本号

- ##### 交互式创建容器		docker run -ti --name=自定义名称 -p端口号：宿主机端口 镜像名称：镜像版本号

- ##### 进入容器		docker exec -it 容器名称/bin/bash

- ##### 通过dockerfile构造镜像		docker built -f dockerfile文件名 -t 自定义文件名 .

- ##### 安装docker compose

  ##### curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose- uname -s -uname -m -o /usr/local/bin/docker-compose

- ##### 设置文件执行权限		chmod +x/use/local/bin/docker-compose

- ##### 查看版本信息		docker-compose -version

- ##### 一键启动多个容器		docker-compose up -d

- ##### 一键关闭多个容器		docker-compose stop

- ##### 一键删除多个容器		docker-compose down

- ##### 远程仓库搭建

  ##### docker run -d \

  #####     --restart=always \

  #####     --name registry	\

  #####     -p 5000:5000 \

  #####     -v registry-data:/var/lib/registry \

  #####     registry

- ##### tag本地镜像		docker tag 本地镜像名：版本号  仓库地址：端口号/新的镜像名：新的镜像版本

- ##### 推送镜像		docker push 仓库地址：端口号/新的镜像名：新的镜像版本号

- ##### 拉取镜像		docker pull 仓库地址 ：端口号/镜像名：镜像版本号